// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyD-DmFvHAOgfqlDINNHfYeqav20hfAy6rE",
    authDomain: "ex-test-56414.firebaseapp.com",
    databaseURL: "https://ex-test-56414.firebaseio.com",
    projectId: "ex-test-56414",
    storageBucket: "ex-test-56414.appspot.com",
    messagingSenderId: "184793256960",
    appId: "1:184793256960:web:cb0ac9a011f81bb16b34e5"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
